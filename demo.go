package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"sync"
)

func main() {
	var urls []string
	filename := flag.String("filename", "images.json", "JSON file with the URL to image")
	workers := flag.Int("workers", 3, "Number of workers for downloading images")
	dir := flag.String("directory", "data/", "Destination directory")
	flag.Parse()
	if len(*filename) == 0 || len(*dir) == 0 {
		fatal(errors.New("invalid filename or directory"))
	}

	_, err := os.Stat(*dir)
	if err != nil {
		if os.IsNotExist(err) {
			err := os.Mkdir(*dir, 0755)
			if err != nil {
				fatal(err)
			}
		} else {
			fatal(err)
		}
	}

	urls = readFile(*filename)

	downloads := make(chan string, *workers)

	var wg sync.WaitGroup

	for id := 1; id <= *workers; id++ {
		go worker(&wg, id, *dir, downloads)
	}

	for _, url := range urls {
		downloads <- url
		wg.Add(1)
	}
	close(downloads)

	wg.Wait()

}

func worker(wg *sync.WaitGroup, id int, dir string, urls <-chan string) {
	for url := range urls {
		err := downloadImage(url, dir, id)
		if err != nil {
			fmt.Printf("%d - Download of image %s failed with error: %s\n", id, url, err)
		}
		wg.Done()
	}
}

func readFile(filename string) []string {
	file, err := os.Open(filename)
	if err != nil {
		fatal(err)
	}
	defer file.Close()
	payload, err := ioutil.ReadAll(file)
	if err != nil {
		fatal(err)
	}
	var images []string
	err = json.Unmarshal(payload, &images)
	if err != nil {
		fatal(err)
	}
	return images
}

func downloadImage(url, dir string, id int) error {

	filename := path.Base(url)
	out := path.Join(dir, filename)
	fmt.Printf("%d - Downloading %s\n", id, url)
	r, err := http.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()
	file, err := os.Create(out)
	if err != nil {
		return err
	}
	defer file.Close()
	_, err = io.Copy(file, r.Body)
	if err != nil {
		return err
	}
	fmt.Printf("%d - Completed %s\n", id, url)
	return nil
}

func fatal(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
	}
	os.Exit(1)
}
